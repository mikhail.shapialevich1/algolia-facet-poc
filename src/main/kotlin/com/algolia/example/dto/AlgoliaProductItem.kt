package com.algolia.example.dto

import kotlinx.serialization.Serializable

@Serializable
data class AlgoliaProductItem(
    val name: String,
    val description: String,
    val price: Long,
    val color: String,
    val size: String
)