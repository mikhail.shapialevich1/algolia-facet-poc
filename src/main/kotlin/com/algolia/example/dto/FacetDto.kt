package com.algolia.example.dto

import com.algolia.search.model.rule.SortRule
import kotlinx.serialization.Serializable

@Serializable
data class FacetDto(
    val name: String,
    val order: List<String>,
    val sortRemainingBy: String
)
