package com.algolia.example.controllers

import com.algolia.example.dto.AlgoliaProductItem
import com.algolia.example.dto.FacetDto
import com.algolia.search.client.Index
import com.algolia.search.dsl.attributesForFaceting
import com.algolia.search.dsl.facets
import com.algolia.search.dsl.query
import com.algolia.search.dsl.settings
import com.algolia.search.model.Attribute
import com.algolia.search.model.response.ResponseSearch
import com.algolia.search.model.response.ResponseSearchForFacets
import com.algolia.search.model.rule.FacetOrdering
import com.algolia.search.model.rule.FacetValuesOrder
import com.algolia.search.model.rule.FacetsOrder
import com.algolia.search.model.rule.RenderingContent
import com.algolia.search.model.rule.SortRule
import com.algolia.search.model.settings.AttributeForFaceting
import io.ktor.util.*
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.jsonObject
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.*
import kotlin.random.Random

@RestController
class AlgoliaController(
    private val index: Index
) {

    // Send generated records to the index
    @PostMapping("/send")
    fun sentItemsToIndex(@RequestParam count: Int) {
        val items = ArrayList<JsonObject>()
        for (i in 1..count) {
            items.add(Json.parseToJsonElement(Json.encodeToString(generateRandomProductItem())).jsonObject)
        }
        Json.parseToJsonElement(Json.encodeToString(generateRandomProductItem()))
        runBlocking {
            index.saveObjects(items, null)
        }
    }

    // initialize first 3 facets for the index
    @PostMapping("/facet/initialize")
    fun addFacet() {
        // Facet ordering and rules
        val facetOrdering = FacetOrdering(
            // Order for facets from this list will be displaying at the search
            facets = FacetsOrder(order = listOf("name", "size", "color")),
            values = mapOf(
                Attribute("name") to FacetValuesOrder(
                    order = listOf("First", "Second", "Bloop", "Pool"),
                    sortRemainingBy = SortRule.Alpha
                ),
                Attribute("size") to FacetValuesOrder(
                    order = listOf("S", "M", "L", "XL"),
                    sortRemainingBy = SortRule.Hidden
                ),
                Attribute("color") to FacetValuesOrder(
                    sortRemainingBy = SortRule.Count
                )
            )
        )
        val rendContent = RenderingContent(facetOrdering)
        val settings = settings {
            renderingContent = rendContent
            //Facet attributes: if we don't add this we will have only ordering but don't have facets in search results
            attributesForFaceting {
                +Searchable("name")
                +FilterOnly("size")
                +"color" //by default attribute will be not searchable
            }
        }

        runBlocking {
            index.setSettings(settings)
        }
    }

    // Add new facet to existed list
    @PostMapping("/facet/new")
    fun addNewFacet(@RequestBody facet: FacetDto) {
        val facetOrdering: FacetOrdering?
        val oldAttributesForFaceting: List<AttributeForFaceting>?
        runBlocking {
            facetOrdering = index.getSettings().renderingContent?.facetOrdering
            oldAttributesForFaceting = index.getSettings().attributesForFaceting

        }

        val oldFacet = facetOrdering!!.facets
        val oldValues: Map<Attribute, FacetValuesOrder> = facetOrdering.values

        val order = listOf(facet.name)
        val values = mapOf(
            Attribute(facet.name) to FacetValuesOrder(
                order = facet.order,
                sortRemainingBy = SortRule.valueOf(facet.sortRemainingBy)
            )
        )

        val newFacetingOrder = FacetOrdering(
            facets = FacetsOrder(order = order.plus(oldFacet.order)),
            values = values.plus(oldValues)
        )
        val rendContent = RenderingContent(newFacetingOrder)
        val atff = oldAttributesForFaceting!!.plus(AttributeForFaceting.Default(Attribute(facet.name)))
        val settings = settings {
            renderingContent = rendContent
            attributesForFaceting = atff

        }
        runBlocking {
            index.setSettings(settings)
        }
    }

    // Got facets from index
    @GetMapping("/facet/list/info")
    fun getFacetList(): FacetOrdering {
        val facets: FacetOrdering
        runBlocking {
            facets = index.getSettings().renderingContent?.facetOrdering!!
        }
        return facets
    }

    @PostMapping("search")
    fun searchHits(@RequestParam query: String): List<ResponseSearch.Hit>? {
        val searchQuery = query(query) {
// This facets should be previously added through API or dashboard: add only needed facets or include all: use *
            facets {
                +"size"
                +"color"
            }
//            facets {
//                +"*"
//            }
        }
        val resultSearch: ResponseSearch
        runBlocking {
            resultSearch = index.search(searchQuery)
        }
        // this is for facets
//        return resultSearch.facetsOrNull
        // this result for hits
        return resultSearch.hitsOrNull
    }

    fun generateRandomProductItem(): AlgoliaProductItem {
        return AlgoliaProductItem(
            name = randomWords[Random.nextInt(randomWords.size)],
            description = UUID.randomUUID().toString(),
            size = sizes[Random.nextInt(sizes.size)],
            color = colors[Random.nextInt(colors.size)],
            price = Random.nextLong(100)
        )
    }

    val sizes = listOf("S", "M", "L", "XL", "SoMuchBig")
    val colors = listOf("blue", "red", "pink", "black", "purple", "green", "yellow")
    val randomWords = listOf("First", "Second", "Bloop", "Pool", "Wood", "Buddha", "Apple", "Other", "Table")
}